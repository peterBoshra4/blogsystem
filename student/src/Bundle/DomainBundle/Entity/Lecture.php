<?php

namespace Bundle\DomainBundle\Entity;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

use Doctrine\ORM\Mapping as ORM;
/**
 * Lecture
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all") 
 */
class Lecture {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    private $id;

    /**
     * @var string
     * @Expose
     * @ORM\Column(name="title", type="string", length=150)
     */
    private $title;

    /**
     * @var string
     * @Expose
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @Expose
     * @ORM\Column(type="datetime")
     */
    protected $lec_time;

    /**
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="lectures")
     */
    protected $module;

    /**
     * @ORM\ManyToMany(targetEntity="\Bundle\UserBundle\Entity\User", mappedBy="lectures")
     **/
    private $users;

    public function __construct() {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Lecture
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Lecture
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }


    /**
     * Set lec_time
     *
     * @param \DateTime $lecTime
     * @return Lecture
     */
    public function setLecTime($lecTime)
    {
        $this->lec_time = $lecTime;

        return $this;
    }

    /**
     * Get lec_time
     *
     * @return \DateTime 
     */
    public function getLecTime()
    {
        return $this->lec_time;
    }

    /**
     * Set module
     *
     * @param \Bundle\DomainBundle\Entity\Module $module
     * @return Lecture
     */
    public function setModule(\Bundle\DomainBundle\Entity\Module $module = null)
    {
        $this->module = $module;

        return $this;
    }

    /**
     * Get module
     *
     * @return \Bundle\DomainBundle\Entity\Module 
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * Add users
     *
     * @param \Bundle\UserBundle\Entity\User $users
     * @return Lecture
     */
    public function addUser(\Bundle\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Bundle\UserBundle\Entity\User $users
     */
    public function removeUser(\Bundle\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }
}
