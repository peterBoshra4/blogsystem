<?php

namespace Bundle\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;



/**
 * Program
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Program {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Module", mappedBy="program")
     */
    protected $modules;

    /**
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="program")
     */
    protected $notifications;
    
    /**
     * @ORM\OneToMany(targetEntity="Topic", mappedBy="program")
     */
    protected $topics;

    /**
     * @ORM\OneToMany(targetEntity="\Bundle\UserBundle\Entity\User", mappedBy="program")
     */
     private $users;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Program
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Program
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->modules = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notifications = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add modules
     *
     * @param \Bundle\DomainBundle\Entity\Module $modules
     * @return Program
     */
    public function addModule(\Bundle\DomainBundle\Entity\Module $modules)
    {
        $this->modules[] = $modules;

        return $this;
    }

    /**
     * Remove modules
     *
     * @param \Bundle\DomainBundle\Entity\Module $modules
     */
    public function removeModule(\Bundle\DomainBundle\Entity\Module $modules)
    {
        $this->modules->removeElement($modules);
    }

    /**
     * Get modules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Add notifications
     *
     * @param \Bundle\DomainBundle\Entity\Notification $notifications
     * @return Program
     */
    public function addNotification(\Bundle\DomainBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;

        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \Bundle\DomainBundle\Entity\Notification $notifications
     */
    public function removeNotification(\Bundle\DomainBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
    
    public function __toString() {
        return $this->title;
    }

    /**
     * Add users
     *
     * @param \Bundle\UserBundle\Entity\User $users
     * @return Program
     */
    public function addUser(\Bundle\UserBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Bundle\UserBundle\Entity\User $users
     */
    public function removeUser(\Bundle\UserBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getUsers()
    {
        return $this->users;
    }

    public function getUsersByRole($role = null) {
        if ($role) {
            return $this->users->filter(function(\Bundle\UserBundle\Entity\User $user) use ($role) {
                return $user->hasRole($role);
            });
        } else {
            return $this->users;
        }
    }

    /**
     * Add topics
     *
     * @param \Bundle\DomainBundle\Entity\Topic $topics
     * @return Program
     */
    public function addTopic(\Bundle\DomainBundle\Entity\Topic $topics)
    {
        $this->topics[] = $topics;

        return $this;
    }

    /**
     * Remove topics
     *
     * @param \Bundle\DomainBundle\Entity\Topic $topics
     */
    public function removeTopic(\Bundle\DomainBundle\Entity\Topic $topics)
    {
        $this->topics->removeElement($topics);
    }

    /**
     * Get topics
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTopics()
    {
        return $this->topics;
    }
}
