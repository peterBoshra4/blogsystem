<?php

namespace Bundle\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;

/**
 * Notification
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table()
 * @ORM\Entity
 * @ExclusionPolicy("all") 
 */
class Notification
{

    const NOTIFICATION_EXAMS = 1;
    const NOTIFICATION_ATTENDANCE = 2;
    const NOTIFICATION_GENERAL = 3;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150)
     * @Expose
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     * @Expose
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="note_type", type="integer", length=25)
     */
    private $note_type;

    /**
     * @ORM\ManyToOne(targetEntity="Program", inversedBy="notifications")
     */
    protected $program;

    /**
     * @ORM\Column(type="datetime")
     * 
     */
    protected $created;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Notification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Notification
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set note_type
     *
     * @param integer $noteType
     * @return Notification
     */
    public function setNoteType($noteType)
    {
        $this->note_type = $noteType;

        return $this;
    }

    /**
     * Get note_type
     *
     * @return integer
     */
    public function getNoteType()
    {
        return $this->note_type;
    }

    /**
     * Set program
     *
     * @param \Bundle\DomainBundle\Entity\Program $program
     * @return Notification
     */
    public function setProgram(\Bundle\DomainBundle\Entity\Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Bundle\DomainBundle\Entity\Program
     */
    public function getProgram()
    {
        return $this->program;
    }


    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Notification
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Now we tell doctrine that before we persist or update we call the updatedTimestamps() function.
     *
     * @ORM\PrePersist
     */
    public function updatedTimestamps()
    {
        $this->setCreated(new \DateTime(date('Y-m-d H:i:s')));

        if ($this->getCreated() == null) {
            $this->setCreated(new \DateTime(date('Y-m-d H:i:s')));
        }
    }

}
