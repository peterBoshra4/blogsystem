<?php

namespace Bundle\DomainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Module
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Module {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="Program", inversedBy="modules")
     */
    protected $program;

    /**
     * @ORM\OneToMany(targetEntity="Lecture", mappedBy="module")
     */
    protected $lectures;

    /**
     * @ORM\ManyToOne(targetEntity="\Bundle\UserBundle\Entity\User", inversedBy="modules")
     */
     private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Bundle\MediaBundle\Entity\Image")
     */
    protected $images;


    public function __construct() {
        $this->lectures = new \Doctrine\Common\Collections\ArrayCollection();
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Module
     */
    public function setTitle($title) {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle() {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Module
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set program
     *
     * @param \Bundle\DomainBundle\Entity\Program $program
     * @return Module
     */
    public function setProgram(\Bundle\DomainBundle\Entity\Program $program = null) {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Bundle\DomainBundle\Entity\Program 
     */
    public function getProgram() {
        return $this->program;
    }


    /**
     * Add lectures
     *
     * @param \Bundle\DomainBundle\Entity\Lecture $lectures
     * @return Module
     */
    public function addLecture(\Bundle\DomainBundle\Entity\Lecture $lectures) {
        $this->lectures[] = $lectures;

        return $this;
    }

    /**
     * Remove lectures
     *
     * @param \Bundle\DomainBundle\Entity\Lecture $lectures
     */
    public function removeLecture(\Bundle\DomainBundle\Entity\Lecture $lectures) {
        $this->lectures->removeElement($lectures);
    }

    /**
     * Get lectures
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLectures() {
        return $this->lectures;
    }

    /**
     * Get todaylectures
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTodayLectures() {
        return $this->lectures->filter(function(\Bundle\DomainBundle\Entity\Lecture $lecture) {
            $today = date('d-m-Y');
            return ((strtotime($today) - strtotime($lecture->getLecTime()->format('d-m-Y'))) == 0 )   ;
        });
        return $this->lectures;
    }

    public function __toString() {
        return $this->title;
    }


    /**
     * Set user
     *
     * @param \Bundle\UserBundle\Entity\User $user
     * @return Module
     */
    public function setUser(\Bundle\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Bundle\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add images
     *
     * @param \Bundle\MediaBundle\Entity\Image $images
     * @return Module
     */
    public function addImage(\Bundle\MediaBundle\Entity\Image $images) {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \Bundle\MediaBundle\Entity\Image $images
     */
    public function removeImage(\Bundle\MediaBundle\Entity\Image $images) {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImages($types = FALSE) {
        if ($types) {
            return $this->images->filter(function($image) use ($types) {
                return in_array($image->getImageType(), $types);
            });
        } else {
            return $this->images;
        }
    }


}
