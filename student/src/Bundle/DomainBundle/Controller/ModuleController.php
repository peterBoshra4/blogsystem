<?php

namespace Bundle\DomainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bundle\DomainBundle\Entity\Module;
use Bundle\DomainBundle\Form\ModuleType;
use Bundle\MediaBundle\Entity\Image;
use Symfony\Component\HttpFoundation\Response;


/**
 * Module controller.
 *
 * @Route("/module")
 */
class ModuleController extends Controller
{

    /**
     * Lists all Module entities.
     *
     * @Route("/", name="module")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $lecturerModules = array();
        $programModules = array();
        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('BundleDomainBundle:Module')->findAll();
        if ($this->getUser()->hasRole("ROLE_LECTURER")){
        if ($this->getUser()->getProgram()){
            $programModules = $this->getUser()->getProgram()->getModules();
        }
            $lecturerModules = $this->getUser()->getModules();
        }
         return array(
            'entities' => $entities,
            'lecturerModules' => $lecturerModules,
            'programModules' => $programModules,

        );
    }

    /**
     * Creates a new Module entity.
     *
     * @Route("/", name="module_create")
     * @Method("POST")
     * @Template("BundleDomainBundle:Module:new.html.twig")
     */
    public function createAction(Request $request)
    {
         $em = $this->getDoctrine()->getManager();
         
        $entity = new Module();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em->persist($entity);
            $em->flush();
        $programId = $request->get('program');
        if($programId != ""){
            
        $program = $em->getRepository('BundleDomainBundle:Program')->find($programId);
        $entity->setProgram($program);
        $em->persist($entity);
        $em->flush();
        }
        $lecturerId = $request->get('lecturer');
        if($lecturerId != ""){
        $lecturer = $em->getRepository('UserBundle:User')->find($lecturerId);
        $entity->setUser($lecturer);
        $em->persist($entity);
        $em->flush();
        }
            return $this->redirect($this->generateUrl('module'));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Module entity.
     *
     * @param Module $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Module $entity)
    {
        $form = $this->createForm(new ModuleType(), $entity, array(
            'action' => $this->generateUrl('module_create'),
            'method' => 'POST',
        ));

//        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Module entity.
     *
     * @Route("/new", name="module_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Module();
        $form = $this->createCreateForm($entity);
        if ($this->getUser()->hasRole("ROLE_LECTURER")){
        if ($this->getUser()->getProgram()){
            $programs = array($this->getUser()->getProgram());
        }
        else{
        $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();
        }
        }
        else{
        $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();
        }
        $lecturers = $em->getRepository('UserBundle:User')->findByRole("ROLE_LECTURER");
        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'programs' =>$programs ,
            'lecturers' =>$lecturers
        );
    }

    /**
     * Finds and displays a Module entity.
     *
     * @Route("/{id}", name="module_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BundleDomainBundle:Module')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Module entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Module entity.
     *
     * @Route("/{id}/edit", name="module_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BundleDomainBundle:Module')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Module entity.');
        }
       if ($this->getUser()->hasRole("ROLE_LECTURER")){
        if ($this->getUser()->getProgram()){
            $programs = array($this->getUser()->getProgram());
        }
        else{
        $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();
        }
        }
        else{
        $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();
        }        $lecturers = $em->getRepository('UserBundle:User')->findByRole("ROLE_LECTURER");

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'programs' => $programs , 
            'lecturers' => $lecturers
        );
    }

    /**
     * Creates a form to edit a Module entity.
     *
     * @param Module $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Module $entity)
    {
        $form = $this->createForm(new ModuleType(), $entity, array(
            'action' => $this->generateUrl('module_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

       // $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Module entity.
     *
     * @Route("/{id}", name="module_update")
     * @Method("PUT")
     * @Template("BundleDomainBundle:Module:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BundleDomainBundle:Module')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Module entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();
         $programId = $request->get('program');
        if($programId != ""){
            
        $program = $em->getRepository('BundleDomainBundle:Program')->find($programId);
        $entity->setProgram($program);
        $em->persist($entity);
        $em->flush();
        }
        $lecturerId = $request->get('lecturer');
        if($lecturerId != ""){
        $lecturer = $em->getRepository('UserBundle:User')->find($lecturerId);
        $entity->setUser($lecturer);
        $em->persist($entity);
        $em->flush();
        }
            return $this->redirect($this->generateUrl('module'));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        );
    }

    /**
     * Deletes a Module entity.
     *
     * @Route("/delete", name="module_delete")
     * @Method("POST")
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BundleDomainBundle:Module')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Module entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('module'));
    }

    /**
     * Creates a form to delete a Module entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('module_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }
    /**
     * Deletes a Module entity.
     *
     * @Route("/upload/files/{id}", name="module_upload_files")
     * @Method("GET")
     */
    public function managefilesAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $module = $em->getRepository('BundleDomainBundle:Module')->find($id);
         $images = $module->getImages();
        return $this->render('BundleDomainBundle:Module:myfiles.html.twig', array(
            'module' => $module ,
            'images' => $images
        ));

    }
    /**
     * Creates a new person image.
     *
     * @Route("/gallery/{id}" , name="module_create_images")
     * @Method("POST")
     * @Template("BundleDomainBundle:Module:myfiles.html.twig")
     */
    public function SetImagesAction(Request $request, $id) {
        $form = $this->createForm(new \Bundle\MediaBundle\Form\ImageType());
        $form->bind($request);

        $data = $form->getData();
        $files = $data["files"];

        $em = $this->getDoctrine()->getManager();
        $module = $em->getRepository('BundleDomainBundle:Module')->find($id);
        if (!$module) {
            throw $this->createNotFoundException('Unable to find package entity.');
        }
        foreach ($files as $file) {
            
            if ($file != NULL) {
                $image = new Image();
                $em->persist($image);
                $em->flush();
                $image->setFile($file);
                $image->setTitle($file->getClientOriginalName());
                $image->setImageType(Image::TYPE_GALLERY);
                $image->preUpload();
                $image->upload("modules/" . $id);
                $module->addImage($image);
                $imageUrl = "/uploads/modules/" . $id . "/" . $image->getId();
                $imageId = $image->getId();
            }

            $em->persist($module);
            $em->flush();
        }
        $files = '{"files":[{"url":"' . $imageUrl . '","thumbnailUrl":"http://lh6.ggpht.com/0GmazPJ8DqFO09TGp-OVK_LUKtQh0BQnTFXNdqN-5bCeVSULfEkCAifm6p9V_FXyYHgmQvkJoeONZmuxkTBqZANbc94xp-Av=s80","name":"test","id":"' . $imageId . '","type":"' . $image->getExtension() . '","size":620888,"deleteUrl":"http://localhost/packagat/web/uploads/packages/1/th71?delete=true","deleteType":"DELETE"}]}';
        $response = new Response();
        $response->setContent($files);
        $response->setStatusCode(200);
        return $response;
    }

    public function deleteOldImage($logo, $module, $image) {
        $em = $this->getDoctrine()->getManager();
        $module->removeImage($logo);
        $em->persist($module);
        $em->flush();
        $image->storeFilenameForRemove("modules/" . $module->getId());
        $image->removeUpload();
        $image->storeFilenameForResizeRemove("modules/" . $module->getId());
        $image->removeResizeUpload();
        $em->persist($logo);
        $em->flush();
        $em->remove($logo);
        $em->flush();
        return true;
    }

    /**
     * Deletes a AttractionGallery entity.
     *
     * @Route("/deleteimage/{h_id}/{redirect_id}", name="moduleimages_delete")
     * @Method("POST")
     */
    public function deleteImageAction($h_id, $redirect_id) {
        $image_id = $this->getRequest()->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BundleDomainBundle:Module')->find($h_id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find module entity.');
        }
        $image = $em->getRepository('MediaBundle:Image')->find($image_id);
        if (!$image) {
            throw $this->createNotFoundException('Unable to find image entity.');
        }
        $entity->removeImage($image);
        $em->persist($entity);
        $em->flush();
        $image->storeFilenameForRemove("modules/" . $h_id);
        $image->removeUpload();
        $image->storeFilenameForResizeRemove("modules/" . $h_id);
        $image->removeResizeUpload();
        $em->persist($image);
        $em->flush();
        $em->remove($image);
        $em->flush();
        return $this->redirect($this->generateUrl('module_upload_files', array('id' => $h_id)));
    }
}
