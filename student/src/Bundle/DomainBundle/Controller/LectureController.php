<?php

namespace Bundle\DomainBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bundle\DomainBundle\Entity\Lecture;
use Bundle\UserBundle\Entity\User;
use Bundle\DomainBundle\Form\LectureType;

/**
 * Lecture controller.
 *
 * @Route("/lecture")
 */
class LectureController extends Controller
{

    /**
     * Lists all Lecture entities.
     *
     * @Route("/{id}", name="lecture")
     * @Method("GET")
     * @Template()
     */
    public function indexAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('BundleDomainBundle:Lecture')->findBy(array('module'=>$id));
        $module = $em->getRepository('BundleDomainBundle:Module')->find($id);


        return array(
            'entities' => $entities,
            'module' => $module
        );
    }

    /**
     * Creates a new Lecture entity.
     *
     * @Route("/", name="lecture_create")
     * @Method("POST")
     * @Template("BundleDomainBundle:Lecture:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = new Lecture();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);
        $module_id = $request->get('module_id');
        $module = $em->getRepository('BundleDomainBundle:Module')->find($module_id);

        if ($form->isValid()) {
            $entity->setModule($module);
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('lecture',array('id' => $module_id)));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Lecture entity.
     *
     * @param Lecture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Lecture $entity)
    {
        $form = $this->createForm(new LectureType(), $entity, array(
            'action' => $this->generateUrl('lecture_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Displays a form to create a new Lecture entity.
     *
     * @Route("/new/lecture/{id}", name="lecture_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction($id)
    {
        $entity = new Lecture();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
            'module_id' => $id
        );
    }

    /**
     * Finds and displays a Lecture entity.
     *
     * @Route("/{id}", name="lecture_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BundleDomainBundle:Lecture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lecture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Lecture entity.
     *
     * @Route("/{id}/edit", name="lecture_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BundleDomainBundle:Lecture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lecture entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'module_id'=>$entity->getModule()->getId()
        );
    }

    /**
     * Creates a form to edit a Lecture entity.
     *
     * @param Lecture $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Lecture $entity)
    {
        $form = $this->createForm(new LectureType(), $entity, array(
            'action' => $this->generateUrl('lecture_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update', 'attr' => array('class' => 'btn btn-primary')));

        return $form;
    }

    /**
     * Edits an existing Lecture entity.
     *
     * @Route("/{id}", name="lecture_update")
     * @Method("PUT")
     * @Template("BundleDomainBundle:Lecture:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('BundleDomainBundle:Lecture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lecture entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('lecture',array('id' => $entity->getModule()->getId())));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Lecture entity.
     *
     * @Route("/delete", name="lecture_delete")
     * @Method("POST")
     */
    public function deleteAction()
    {
        $id = $this->getRequest()->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('BundleDomainBundle:Lecture')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Lecture entity.');
        }

        $em->remove($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('lecture',array('id' => $entity->getModule()->getId())));
    }

    /**
     * Creates a form to delete a Lecture entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('lecture_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

    /**
     * Show the page of attendance in lecture
     *
     * @Route("/manage_attendance/{id}", name="lecture_attendance")
     * @Method("GET")
     * @Template()
     */
    public function attendanceAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $lecture = $em->getRepository('BundleDomainBundle:Lecture')->find($id);
        $lectureStudents = $lecture->getModule()->getProgram()->getUsersByRole('ROLE_STUDENT');
        $attendanceStudents = $lecture->getUsers();
        $studentsIds = array();
        foreach($attendanceStudents as $student){
            $studentsIds[] = $student->getId();
        }

        return array(
            'students' => $lectureStudents,
            'lecture' => $lecture,
            'students_ids' => $studentsIds
        );
    }


    /**
     * Creates a new Lecture entity.
     *
     * @Route("/attendance_create", name="attendance_create")
     * @Method("POST")
     * @Template("BundleDomainBundle:Lecture:attendance.html.twig")
     */
    public function attendanceCreateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $students = $request->get('students');
        $lecture_id = $request->get('lecture_id');
        $lecture = $em->getRepository('BundleDomainBundle:Lecture')->find($lecture_id);

        $attendanceStudents = $lecture->getUsers();

        foreach($attendanceStudents as $student){
            $lecture->removeUser($student);
            $student->removeLecture($lecture);
            $em->persist($lecture);
            $em->persist($student);
            $em->flush();
        }


        if($students) {
            foreach ($students as $student) {
                $user = $em->getRepository('UserBundle:User')->find($student);
                $lecture->addUser($user);
                $user->addLecture($lecture);

                $em->persist($lecture);
                $em->persist($user);
                $em->flush();
            }
        }

        $attendanceStudents = $lecture->getUsers();
        $studentsIds = array();
        foreach($attendanceStudents as $student){
            $studentsIds[] = $student->getId();
        }

        $lecture = $em->getRepository('BundleDomainBundle:Lecture')->find($lecture_id);
        $lectureStudents = $lecture->getModule()->getProgram()->getUsersByRole('ROLE_STUDENT');


        return array(
            'students' => $lectureStudents,
            'lecture' => $lecture,
            'students_ids' => $studentsIds
        );
    }

}
