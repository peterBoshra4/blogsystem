<?php

namespace Bundle\DomainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class NotificationType extends AbstractType {

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('title')
                ->add('description')
                ->add('note_type', 'choice', array(
                    'choices' => array(
                        '1' => 'Exams',
                        '2' => 'Attendance',
                        '3' => 'General'
                    ),
                ))
                ->add('program')
        ;
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'Bundle\DomainBundle\Entity\Notification'
        ));
    }

    /**
     * @return string
     */
    public function getName() {
        return 'bundle_domainbundle_notification';
    }

}
