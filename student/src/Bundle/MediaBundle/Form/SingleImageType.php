<?php

namespace Bundle\MediaBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SingleImageType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->
                add('file', 'file', array(
                    "required" => FALSE))
                ->getForm();
    }

    public function getName() {
        return '';
    }

}
