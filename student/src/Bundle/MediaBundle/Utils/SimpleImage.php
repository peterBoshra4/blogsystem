<?php

/*
 * File: SimpleImage.php
 * Author: Simon Jarvis
 * Copyright: 2006 Simon Jarvis
 * Date: 08/11/06
 * Link: http://www.white-hat-web-design.co.uk/articles/php-image-resizing.php
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details:
 * http://www.gnu.org/licenses/gpl.html
 *
 */

namespace Bundle\MediaBundle\Utils;

class SimpleImage {

    var $image;
    var $image_type;

    function load($filename) {

        $image_info = getimagesize($filename);
        $this->image_type = $image_info[2];
        if ($this->image_type == IMAGETYPE_JPEG) {

            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->image_type == IMAGETYPE_GIF) {

            $this->image = imagecreatefromgif($filename);
        } elseif ($this->image_type == IMAGETYPE_PNG) {

            $this->image = imagecreatefrompng($filename);
        }
    }

    function save($filename, $compression = 100, $image_type = IMAGETYPE_JPEG, $permissions = null) {

        if ($this->image_type == IMAGETYPE_JPEG) {
            imagejpeg($this->image, $filename, $compression);
        } elseif ($this->image_type == IMAGETYPE_GIF) {
            imagegif($this->image, $filename);
        } elseif ($this->image_type == IMAGETYPE_PNG) {

            imagepng($this->image, $filename);
        }
        if ($permissions != null) {

            chmod($filename, $permissions);
        }
    }

    function output($image_type = IMAGETYPE_JPEG) {

        if ($image_type == IMAGETYPE_JPEG) {
            imagejpeg($this->image);
        } elseif ($image_type == IMAGETYPE_GIF) {

            imagegif($this->image);
        } elseif ($image_type == IMAGETYPE_PNG) {

            imagepng($this->image);
        }
    }

    function getWidth() {

        return imagesx($this->image);
    }

    function getHeight() {

        return imagesy($this->image);
    }

    function resizeToHeight($height) {

        $ratio = $height / $this->getHeight();
        $width = $this->getWidth() * $ratio;
        $this->resize($width, $height);
    }

    function resizeToWidth($width) {
        $ratio = $width / $this->getWidth();
        $height = $this->getheight() * $ratio;
        $this->resize($width, $height);
    }

    function scale($scale) {
        $width = $this->getWidth() * $scale / 100;
        $height = $this->getheight() * $scale / 100;
        $this->resize($width, $height);
    }

    function resize($width, $height) {
        $new_image = imagecreatetruecolor($width, $height);
        switch ($this->image_type) {
            case IMAGETYPE_PNG :
                // integer representation of the color black (rgb: 0,0,0)
                $background = imagecolorallocate($new_image, 0, 0, 0);
                // removing the black from the placeholder
                imagecolortransparent($new_image, $background);
                // turning off alpha blending (to ensure alpha channel information 
                // is preserved, rather than removed (blending with the rest of the 
                // image in the form of black))
                imagealphablending($new_image, false);

                // turning on alpha channel information saving (to ensure the full range 
                // of transparency is preserved)
                imagesavealpha($new_image, true);

                break;
            case IMAGETYPE_GIF:
                // integer representation of the color black (rgb: 0,0,0)
                $background = imagecolorallocate($new_image, 0, 0, 0);
                // removing the black from the placeholder
                imagecolortransparent($new_image, $background);

                break;
        }
        imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->getWidth(), $this->getHeight());
        $this->image = $new_image;
    }

    /* added by fady */

    public static function saveNewResizedImages($imagePath, $newImagePath, $maxWidth, $maxHeight, $optimization = 100, $bigImagePath = null, $bigImageMaxWidth = 0, $bigImageMaxHeight = 0) {
        self::saveNewResizedImage($imagePath, $newImagePath, $maxWidth, $maxHeight);
        if (!is_null($bigImagePath))
            self::saveNewResizedImage($imagePath, $bigImagePath, $bigImageMaxWidth, $bigImageMaxHeight, $optimization);
        Files::deleteFile($imagePath);
        rename($newImagePath, $imagePath);
    }

    public static function saveNewResizedImage($imagePath, $newImagePath, $maxWidth, $maxHeight, $optimization = 100) {
//       exit('<img src="'.$imagePath.'">');
        $image = new SimpleImage();
        $image->load($imagePath);
        if ($image->getWidth() >= $image->getHeight())
            $image->resizeToWidth($maxWidth);
        else
            $image->resizeToHeight($maxHeight);
        $image->save($newImagePath, $optimization);
    }

}

?>