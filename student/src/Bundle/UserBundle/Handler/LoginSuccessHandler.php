<?php

namespace Bundle\UserBundle\Handler;

use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Router;

class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface {

    protected $router;
    protected $security;

    public function __construct(Router $router, SecurityContext $security) {
        $this->router = $router;
        $this->security = $security;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token) {

        if ($this->security->isGranted('ROLE_ADMIN')) {
            $response = new RedirectResponse($this->router->generate('list_students'));
        } elseif ($this->security->isGranted('ROLE_STUDENT')) {
            $response = new RedirectResponse($this->router->generate('homepage'));
            // redirect the user to where they were before the login process begun.
           //    $response = new RedirectResponse($this->router->generate('person'));
        } elseif ($this->security->isGranted('ROLE_LECTURER')) {
            $response = new RedirectResponse($this->router->generate('lechomepage'));
            // redirect the user to where they were before the login process begun.
           //    $response = new RedirectResponse($this->router->generate('person'));
        }

        return $response;
    }

}