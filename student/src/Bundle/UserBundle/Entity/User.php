<?php

namespace Bundle\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Expose;
use JMS\Serializer\Annotation\Groups;
use JMS\Serializer\Annotation\VirtualProperty;


/**
 * @ORM\Entity(repositoryClass="Bundle\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 * @ExclusionPolicy("all") 
 */
class User extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Expose
     */
    protected $id;


    /**
     * @ORM\OneToOne(targetEntity="Profile" , mappedBy="id")
     * @Expose
     * */
    protected $profile;

    /**
     * @ORM\ManyToOne(targetEntity="\Bundle\DomainBundle\Entity\Program", inversedBy="users")
     * @ORM\JoinColumn(name="program_id", referencedColumnName="id")
     * @Expose
     */
    private $program;
    
    /**
     * @ORM\OneToMany(targetEntity="\Bundle\DomainBundle\Entity\Module",  mappedBy="user")
     */
    private $modules;


    /**
     * @ORM\ManyToMany(targetEntity="\Bundle\DomainBundle\Entity\Lecture", inversedBy="users")
     * @ORM\JoinTable(name="student_attendance")
     **/
    private $lectures;
    
    /**
     * @ORM\OneToMany(targetEntity="\Bundle\DomainBundle\Entity\Comment", mappedBy="user")
     */
    protected $comments;


    public function __construct() {
        parent::__construct();
        $this->lectures = new \Doctrine\Common\Collections\ArrayCollection();
        $this->modules = new \Doctrine\Common\Collections\ArrayCollection();
        }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set program
     *
     * @param \Bundle\DomainBundle\Entity\Program $program
     * @return User
     */
    public function setProgram(\Bundle\DomainBundle\Entity\Program $program = null)
    {
        $this->program = $program;

        return $this;
    }

    /**
     * Get program
     *
     * @return \Bundle\DomainBundle\Entity\Program 
     */
    public function getProgram()
    {
        return $this->program;
    }

    /**
     * Set profile
     *
     * @param \Bundle\UserBundle\Entity\Profile $profile
     * @return User
     */
    public function setProfile(\Bundle\UserBundle\Entity\Profile $profile = null)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return \Bundle\UserBundle\Entity\Profile 
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Add lectures
     *
     * @param \Bundle\DomainBundle\Entity\Lecture $lectures
     * @return User
     */
    public function addLecture(\Bundle\DomainBundle\Entity\Lecture $lectures)
    {
        $this->lectures[] = $lectures;

        return $this;
    }

    /**
     * Remove lectures
     *
     * @param \Bundle\DomainBundle\Entity\Lecture $lectures
     */
    public function removeLecture(\Bundle\DomainBundle\Entity\Lecture $lectures)
    {
        $this->lectures->removeElement($lectures);
    }

    /**
     * Get lectures
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLectures()
    {
        return $this->lectures;
    }

    /**
     * Add modules
     *
     * @param \Bundle\DomainBundle\Entity\Module $modules
     * @return User
     */
    public function addModule(\Bundle\DomainBundle\Entity\Module $modules)
    {
        $this->modules[] = $modules;

        return $this;
    }

    /**
     * Remove modules
     *
     * @param \Bundle\DomainBundle\Entity\Module $modules
     */
    public function removeModule(\Bundle\DomainBundle\Entity\Module $modules)
    {
        $this->modules->removeElement($modules);
    }

    /**
     * Get modules
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * Add comments
     *
     * @param \Bundle\DomainBundle\Entity\Comment $comments
     * @return User
     */
    public function addComment(\Bundle\DomainBundle\Entity\Comment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \Bundle\DomainBundle\Entity\Comment $comments
     */
    public function removeComment(\Bundle\DomainBundle\Entity\Comment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getComments()
    {
        return $this->comments;
    }
    


    
}
