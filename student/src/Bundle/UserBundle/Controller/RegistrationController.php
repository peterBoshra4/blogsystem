<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Bundle\UserBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\FormEvent;
use Bundle\UserBundle\Entity\Profile As Profile;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use FOS\UserBundle\Model\UserInterface;

/**
 * Controller managing the registration
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class RegistrationController extends Controller
{

    /*
     * show add student twig file
     */
    public function createstudentAction()
    {
        $em = $this->getDoctrine()->getManager();
        $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();
        return $this->render('FOSUserBundle:Registration:createstudent.html.twig', array(
            'programs' => $programs
        ));
    }
    /*
     * show add student twig file
     */
    public function createlecturerAction()
    {
         $em = $this->getDoctrine()->getManager();
        $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();
       $modules = $em->getRepository('BundleDomainBundle:Module')->findAll();

        return $this->render('FOSUserBundle:Registration:createlecturer.html.twig', array(
                        'programs' => $programs,
                        'modules' => $modules
        ));
    }
    /*
    * insert student with role studennt to the database
    */
    public function insertstudentAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $email = $request->get('email');
        $name = $request->get('name');
        $address = $request->get('address');
        $phone = $request->get('phone');
        $password = $request->get('password');
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername($email);
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole("ROLE_STUDENT");
        $userManager->updateUser($user);
        $profile = new Profile();
        $profile->setId($user);
        $profile->setName($name);
        $profile->setAddress($address);
        $profile->setPhone($phone);
        $em->persist($profile);
        $em->flush();

        $programId = $request->get('program');
        if($programId != ""){
            
        $program = $em->getRepository('BundleDomainBundle:Program')->find($programId);
        $user->setProgram($program);
        $em->persist($user);
        $em->flush();
        }
        return $this->redirect($this->generateUrl('list_students', array()));
    }
    /*
    * insert student with role studennt to the database
    */
    public function insertlecturerAction()
    {
        $em = $this->getDoctrine()->getManager();
        $request = $this->getRequest();
        $email = $request->get('email');
        $name = $request->get('name');
        $address = $request->get('address');
        $phone = $request->get('phone');
        $password = $request->get('password');
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername($email);
        $user->setEmail($email);
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole("ROLE_LECTURER");
        $userManager->updateUser($user);
        $profile = new Profile();
        $profile->setId($user);
        $profile->setName($name);
        $profile->setAddress($address);
        $profile->setPhone($phone);
        $em->persist($profile);
        $em->flush();
        $programId = $request->get('program');
        if($programId != ""){
        $program = $em->getRepository('BundleDomainBundle:Program')->find($programId);
        $user->setProgram($program);
        $em->persist($user);
        $em->flush();
        }
        return $this->redirect($this->generateUrl('list_lecturers', array()));
    }
    /*
     * show edit student twig file
     * parameter id to get  the student with it
     */
    public function editstudentAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $student = $em->getRepository('UserBundle:User')->find($id);
        $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();
        return $this->render('FOSUserBundle:Registration:editstudent.html.twig', array(
            "user" => $student , 
            "programs" => $programs
        ));
    }
    /*
     * update student info in the database
     * parameter id to get  the student with it
     */
    public function updatestudentAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        $request = $this->getRequest();
        $email = $request->get('email');
        $name = $request->get('name');
        $address = $request->get('address');
        $phone = $request->get('phone');
        $password = $request->get('password');
        $userManager = $this->get('fos_user.user_manager');
        $user->setUsername($email);
        $user->setEmail($email);
        if ($password != "")
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole("ROLE_STUDENT");
        $userManager->updateUser($user);
        if ($user->getProfile())
        {
        $profile = $user->getProfile() ;
            $profile->setName($name);
            $profile->setAddress($address);
            $profile->setPhone($phone);
            $em->persist($profile);
        }
        else {
            $profile = new Profile();
            $profile->setId($user);
            $profile->setName($name);
            $profile->setAddress($address);
            $profile->setPhone($phone);
            $em->persist($profile);
        }
        $em->flush();
        $programId = $request->get('program');
        if($programId != ""){
            
        $program = $em->getRepository('BundleDomainBundle:Program')->find($programId);
        $user->setProgram($program);
        $em->persist($user);
        $em->flush();
        }
        return $this->redirect($this->generateUrl('list_students', array()));
    }
 /*
     * show edit student twig file
     * parameter id to get  the student with it
     */
    public function editlecturerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $lecturer = $em->getRepository('UserBundle:User')->find($id);
          $programs = $em->getRepository('BundleDomainBundle:Program')->findAll();

        return $this->render('FOSUserBundle:Registration:editlecturer.html.twig', array(
            "user" => $lecturer , 
             'programs' => $programs,

        ));
    }
    /*
     * update student info in the database
     * parameter id to get  the student with it
     */
    public function updatelecturerAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('UserBundle:User')->find($id);
        $request = $this->getRequest();
        $email = $request->get('email');
        $name = $request->get('name');
        $address = $request->get('address');
        $phone = $request->get('phone');
        $password = $request->get('password');
        $userManager = $this->get('fos_user.user_manager');
        $user->setUsername($email);
        $user->setEmail($email);
        if ($password != "")
        $user->setPlainPassword($password);
        $user->setEnabled(true);
        $user->addRole("ROLE_LECTURER");
        $userManager->updateUser($user);
        if ($user->getProfile())
        {
        $profile = $user->getProfile() ;
            $profile->setName($name);
            $profile->setAddress($address);
            $profile->setPhone($phone);
            $em->persist($profile);
        }
        else {
            $profile = new Profile();
            $profile->setId($user);
            $profile->setName($name);
            $profile->setAddress($address);
            $profile->setPhone($phone);
            $em->persist($profile);
        }
        $em->flush();
      $programId = $request->get('program');
        if($programId != ""){
            
        $program = $em->getRepository('BundleDomainBundle:Program')->find($programId);
        $user->setProgram($program);
        $em->persist($user);
        $em->flush();
        }
        return $this->redirect($this->generateUrl('list_lecturers', array()));
    }

    public function liststudentsAction()
    {
        $em = $this->getDoctrine()->getManager();
        $students = $em->getRepository('UserBundle:User')->findByRole("ROLE_STUDENT");
        return $this->render('FOSUserBundle:Registration:students.html.twig', array(
            'users' => $students
        ));
    }
    public function listlecturersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $lecturers = $em->getRepository('UserBundle:User')->findByRole("ROLE_LECTURER");
        return $this->render('FOSUserBundle:Registration:lecturers.html.twig', array(
            'users' => $lecturers
        ));
    }
    public function registerAction(Request $request)
    {
        /** @var $formFactory \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');
        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm();
        $form->setData($user);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $event = new FormEvent($form, $request);
            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

            $userManager->updateUser($user);

//            if (null === $response = $event->getResponse()) {
//                $url = $this->generateUrl('fos_user_registration_confirmed');
//                $response = new RedirectResponse($url);
//            }

//            $dispatcher->dispatch(FOSUserEvents::REGISTRATION_COMPLETED, new FilterUserResponseEvent($user, $request, $response));
//
//            return $response;
        }

        return $this->render('FOSUserBundle:Registration:register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction()
    {
        $email = $this->get('session')->get('fos_user_send_confirmation_email/email');
        $this->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

        return $this->render('FOSUserBundle:Registration:checkEmail.html.twig', array(
            'user' => $user,
        ));
    }

    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction(Request $request, $token)
    {
        /** @var $userManager \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        /** @var $dispatcher \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user->setConfirmationToken(null);
        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRM, $event);

        $userManager->updateUser($user);

        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('fos_user_registration_confirmed');
            $response = new RedirectResponse($url);
        }

        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_CONFIRMED, new FilterUserResponseEvent($user, $request, $response));

        return $response;
    }

    /**
     * Tell the user his account is now confirmed
     */
    public function confirmedAction()
    {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        return $this->render('FOSUserBundle:Registration:confirmed.html.twig', array(
            'user' => $user,
        ));
    }
}
