<?php

namespace Bundle\UserBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Bundle\DomainBundle\Entity\Lecture;

class UserRestController extends Controller {

    public function getUserAction($username) {
        $user = $this->getUser();
        if (!is_object($user)) {
            throw $this->createNotFoundException();
        }
        return $user->getProgram();
    }

    public function getMyNotiAction() {
        $user = $this->getUser();
        if (!is_object($user)) {
            throw $this->createNotFoundException();
        }
        return $user->getProgram()->getNotifications();
    }

    public function getMyLecturesAction() {

        $modules = $this->getUser()->getProgram()->getModules();
        $lectures = array();
        foreach ($modules as $module) {
            foreach ($module->getLectures() as $lecture) {
                $lectures[] = $lecture;
            }
        }

        return $lectures;
    }

}
