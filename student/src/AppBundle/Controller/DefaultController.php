<?php

namespace AppBundle\Controller;

use Bundle\DomainBundle\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $user_id = $this->getUser()->getId();
        $attended = $this->getUser()->getLectures();
        $topics = $this->getUser()->getProgram()->getTopics();
        $lectures_attended = array();

        foreach ($attended as $lecture) {
            $lectures_attended[] = $lecture->getId();
        }

        if ($this->getUser()->getProgram()) {
            $modules = $this->getUser()->getProgram()->getModules();
        } else {
            $modules = Null;
        }


        $lectures = array();
        foreach ($modules as $module) {
            foreach ($module->getLectures() as $lecture) {
                $lectures[] = $lecture;
            }
        }

        $notifications = $this->getUser()->getProgram()->getNotifications();

        return $this->render('FOSUserBundle:Registration:dashboard.html.twig', array(
            'userId' => $user_id,
            'lectures' => $lectures,
            'lectures_attended' => $lectures_attended,
            'notifications' => $notifications,
            'topics' => $topics,
        ));
    }
    public function lecturerdashboardAction()
    {
        return $this->render('BundleDomainBundle:Module:lecdashboard.html.twig', array(
        ));
    }


    public function createAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $comment = $request->get('comment_body');
        $topic_id = $request->get('topic_id');
        $topic = $em->getRepository('BundleDomainBundle:Topic')->find($topic_id);
        $entity = new Comment();
        $entity->setDescription($comment);
        $entity->setTopic($topic);
        $entity->setUser($this->getUser());
        $em->persist($entity);
        $em->flush();

        $user_id = $this->getUser()->getId();
        $attended = $this->getUser()->getLectures();
        $topics = $this->getUser()->getProgram()->getTopics();
        $lectures_attended = array();

        foreach ($attended as $lecture) {
            $lectures_attended[] = $lecture->getId();
        }

        if ($this->getUser()->getProgram()) {
            $modules = $this->getUser()->getProgram()->getModules();
        } else {
            $modules = Null;
        }


        $lectures = array();
        foreach ($modules as $module) {
            foreach ($module->getLectures() as $lecture) {
                $lectures[] = $lecture;
            }
        }

        $notifications = $this->getUser()->getProgram()->getNotifications();


        return $this->render('FOSUserBundle:Registration:dashboard.html.twig', array(
            'userId' => $user_id,
            'lectures' => $lectures,
            'lectures_attended' => $lectures_attended,
            'notifications' => $notifications,
            'topics' => $topics,
        ));
    }

    public function moduleContentAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $module = $em->getRepository('BundleDomainBundle:Module')->find($id);
        $images = $module->getImages();
        return $this->render('FOSUserBundle:Registration:moduleContent.html.twig', array(
            'module' => $module ,
            'images' => $images
        ));
    }


}
